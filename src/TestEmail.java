

import org.junit.*;
import static org.junit.Assert.*;


/** Test case class for testing emails address verification.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TestEmail {

  private EmailAddress address;

  @Test
  public void testInput() {
    assertTrue((new EmailAddress("kaley@cs.edu")).isValid());
    assertFalse((new EmailAddress("12kaley@cs.edu")).isValid());
  }

  @Test
  public void shouldAcceptJohnAtCsDotEdu() {
    EmailAddress ea = new EmailAddress("john@cs.edu");
    assertTrue( ea.isValid() );
  }
  @Test
  public void shouldNotAccept123AtCsDotEdu() {
    EmailAddress ea = new EmailAddress("123@cs.edu");
    assertFalse( ea.isValid() );
  }
}

